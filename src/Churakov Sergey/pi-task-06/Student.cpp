#include "Student.h"
#include "Group.h"
#include <string>
#include <iostream>
using namespace::std;
Student::Student(void)
{
	ID = 0;
	num = 0;
	Fio = " ";
	group = nullptr;
	marks = nullptr;
}
Student::Student (int _ID, string _Fio, Group* _group): ID(_ID), Fio(_Fio)
{
	num = 0;
	marks = nullptr;
	group= _group;
}

Student::Student (Student& st)
{
	ID = st.ID;
	Fio = st.Fio;
	num = st.num;
	group = st.group;
	for (int i = 0; i<10; i++)
		marks[i] = st.marks[i];
}
int Student:: GetID(void)
{
	return ID;
}
int Student:: getNum(void)
{    return num;     }  
string Student:: getFio(void)
{
	return Fio;
}
 Group* Student:: getGroup(void)
{
	return group;
}
void Student:: AddMark(int mark)
{
	if (num == 0)
		{
		marks = new int[1];
		marks[num] = mark;
		}
	else
		 {
		int* tmp = new int[num + 1];
		for (int i = 0; i < num; i++)
			 tmp[i] = marks[i];
		tmp[num] = mark;
		delete[] marks;
		marks = tmp;
		}
	num++;
	}

void Student:: SetGroup(Group* _group)
{
	group= _group;
}
void Student::SetID(int _id)
{
	ID = _id;
}
void Student::SetFio(string _fio)
{
	Fio = _fio;
}
void Student::SetNum(int _num)
{
	num = _num;
}
double Student:: getAvMark()
{
	if (num == 0)
		return 0;
	double n= 0;
	int i;
	for(i = 0; i<num; i++)
		n+=marks[i];
	n/=i;
	return n;
}

void Student::PrintMarks(ostream & stream)
{
	for (int i = 0; i < num; i++)
		{
		stream << marks[i] << " ";
		}
	stream << endl;
}

void Student::PrintStudent(void)
{
	cout << ID << " " << Fio << " " << num << endl;
	PrintMarks(cout);
	cout << endl;
}

Student::~Student(void)
{
	ID = 0;
	num = 0;
	Fio =' ';
	delete [] marks;
	marks = nullptr;
}
