#pragma once
#include <string>
#include <iostream>
using namespace::std;

class Student;
class Group
{
private:
	string Title;
	int num;
	Student* head;
	Student** staff;
public:
	Group(void);
	Group(Group& gr);
	Group(string _Title) ;
	string GetTitle (void);
	void SetTitle (string _Title);
	int GetNum (void);
	int GetIdbyNum(int _num);
	void AddStud(Student* st);
	int FindStud (int _ID);
	void RemoveStud(int _ID);
	void SetHead (Student* _head);
	void SetRandomHead();
	int GetHead (void);
	double getAvMark (void);
	double getOnesAvMark(int _num);
	void PrintGroup(ostream& stream);
	~Group(void);
};

