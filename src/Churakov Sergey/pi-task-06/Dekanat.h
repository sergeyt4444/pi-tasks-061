#pragma once
#include <iostream>
#include <string>
using namespace::std;

class Group;
class Student;
class Dekanat
{
private:
	Group* allGroups[10];
	Student* allStudents[200];
	int GroupNum;
	int StudNum;
	Student* FindStud(int _id);
	Group* FindGroup(string _title);
public:
	Dekanat(void);
	~Dekanat(void);
	void AddStud(Student* st);
	void AddGroup(Group* gr);
	void RandomizeHeads();
	void AddRandomMarks();
	void GetGroupList(string File);
	void GetStudList(string File);
	void PrintGroupList(string File);
	void PrintStudList(string File);
	void getStats();
	void MoveStud(int _id, Group* _group);
	void RemoveStud(int _id);
	void ChooseHead (int _num, int _id);
	void PrintAll();
	void PrintAllStudents();
	void RemoveBadStudents(int _mark);
	void ChooseHeadbyMarks(int _num);
};
