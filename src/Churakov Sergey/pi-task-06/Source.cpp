#include "Dekanat.h"
#include "Student.h"
#include "Group.h"
#include <string>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <time.h>

using namespace::std;

int main()
{
	srand((int)time(NULL));
	setlocale(LC_ALL, "Russian");
	Dekanat dek;
	dek.GetGroupList("Groups.txt");
	dek.GetStudList("Students.txt");
	dek.AddRandomMarks();
	for (int i = 0; i < 5;i++)
		dek.ChooseHeadbyMarks(i);
	dek.getStats();
	dek.RemoveBadStudents(3);
	dek.getStats();
	dek.PrintGroupList("GroupsOut.txt");
	dek.PrintStudList("StudOut.txt");
	return 0;
}