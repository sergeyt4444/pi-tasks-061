#pragma once
#include <iostream>
#include <string>
using namespace::std;

class Group;
class Student
{
private:
	int ID;
	string Fio;
	int num;
	int* marks;
	Group* group;
public:
	Student(void);
	Student (int _ID, string _Fio, Group* _group);
	Student (Student& st);
	int GetID(void);
	int getNum(void);
	string getFio(void);
	 Group* getGroup(void);
	void AddMark(int mark);
	void SetGroup(Group* _group);
	void SetID(int _id);
	void SetFio(string _fio);
	void SetNum(int _num);
	double getAvMark();
	void PrintMarks(ostream& stream);
	void PrintStudent();
	~Student(void);
};

