#include "Group.h"
#include "Student.h"
#include <string>
#include <iostream>
#include <time.h>
using namespace::std;



Group::Group(void): num (0)
{
	Title = " ";
	head = nullptr;
	staff = new Student*[30];
}

Group::Group (string _Title) : Title(_Title), num(0)
{
	head = nullptr;
	staff = new Student*[30];
}

Group::Group(Group& gr)
{
	head = new Student;
	staff = new Student*[30];
	Title = gr.Title;
	num = gr.num;
	(Student*)head = gr.head;
	for (int i = 0; i < num; i++)
		(Student*)staff[i] = gr.staff[i];
}

string Group:: GetTitle (void)
{
	return (Title);
}

void Group:: SetTitle (string _Title)
{
	Title = _Title;
}

int Group:: GetNum (void)
{
	return num;
}


void Group:: AddStud(Student* st)
{
	staff[num++] = st;
	st->SetGroup(this);
}

int Group:: FindStud (int _ID)
{
	int stnum=-1;
	for (int i=0; i<num; i++)
		if (staff[i]!=nullptr)
		if (staff[i]->GetID() == _ID)
		{
			stnum = i;
			break;
		}
	return stnum;
}

void Group:: RemoveStud(int _ID)
{
	Student* removedst = staff[FindStud (_ID)];
	if (removedst != nullptr)
	{
		int i = 0;
		while (staff[i++]->GetID() != removedst->GetID());
		staff[i - 1] = staff[num - 1];
		if (head == removedst)
			{
			head = nullptr;
			}
		num--;
	}
}

void Group::  SetHead (Student* _head)
{
	head = _head;
}

void Group::SetRandomHead()
{
	srand((int)time(NULL));
	if (num != 0)
		 head = staff[rand() % num];
}

int Group:: GetHead (void)
{
	return head->GetID();
}

double Group:: getAvMark (void)
{
	if (num == 0)
		return 0;
	double avMark=0;
	for (int i = 0; i<num; i++)
	{
		avMark += staff[i]->getAvMark();
	}
	return (avMark/(double)num);
}

void Group::PrintGroup(ostream& stream)
{
	stream << "Title: " << Title << endl;
	stream << "Number of students: " << num << endl;
	if (head != nullptr)
	stream << "Head's ID: " << head->GetID() << endl;
	for (int i = 0; i < num; i++)
		stream << staff[i]->getFio() << endl;
}


double Group::getOnesAvMark(int _num)
{
	return staff[_num]->getAvMark();
}

int Group:: GetIdbyNum(int _num)
{
	return staff[_num]->GetID();
}


Group::~Group(void)
{
	Title = ' ';
	num = 0;
	delete [] staff;
	staff = nullptr;
}
