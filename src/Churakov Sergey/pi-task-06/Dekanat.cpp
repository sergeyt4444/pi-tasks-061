#include "Dekanat.h"
#include "Student.h"
#include "Group.h"
#include <string>
#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace::std;

Dekanat::Dekanat(void): GroupNum(0), StudNum(0)
{
}


Dekanat::~Dekanat(void)
{
	for (int i = 0; i < StudNum; i++)
	{
		delete allStudents[i];
	}
	for (int i = 0; i < GroupNum; i++)
	{
		delete allGroups[i];
	}
	GroupNum = 0;
	StudNum = 0;
}

void Dekanat::AddStud(Student * st)
{
	allStudents[StudNum] = st;
	StudNum++;
}

void Dekanat::AddGroup(Group * gr)
{
	allGroups[GroupNum] = gr;
	GroupNum++;
}

void Dekanat::RandomizeHeads()
{
	for (int i = 0; i < GroupNum; i++)
		allGroups[i]->SetRandomHead();
}


Student* Dekanat:: FindStud(int _id)
{
	for(int i=0; i<StudNum;i++)
		if(allStudents[i]->GetID()==_id)
			return allStudents[i];
	std::cout<<"Error"<<endl;
	exit(EXIT_FAILURE);
}

Group* Dekanat:: FindGroup(string _title)
{
	for(int i=0; i<GroupNum;i++)
		if(allGroups[i]->GetTitle()==_title)
			return allGroups[i];
	std::cout <<"Error"<<endl;
	exit(EXIT_FAILURE);
}


void Dekanat:: AddRandomMarks()
{
	int j, arr[10];
	for (j=0; j<10; j++)
		arr[j] = rand()%4+2;
	for(int i=0; i<StudNum; i++)
	{		
		for (j = 0; j < 10; j++)
		{
			arr[j] = rand() % 4 + 2;
			allStudents[i]->AddMark(arr[j]);
		}
	}
}
void Dekanat::GetGroupList(string File)
{
	ifstream file;
	Group _gr;
	string _title;
	file.open(File);
	if (file.is_open())
		 {
		while (!file.eof())
			 {
			getline(file, _title);
			AddGroup(new Group(_title));
			}
		}
	file.close();
}

void Dekanat::GetStudList(string File)
{
	ifstream file;
	int i = 0;
//	Student* _st = new Student;
	Group* _gr = new Group;
	int _id;
	string _f, _i, _o, grTitle, _fio;
	file.open(File);
	if (file.is_open())
	{
		while (!file.eof())
		{
			file >> _id >> _f >> _i >> _o >> grTitle;
			_fio = _f + " " + _i + " " + _o;
			_gr = FindGroup(grTitle);
			AddStud(new Student(_id, _fio, _gr));
//			_st->SetID(_id);
//			_st->SetFio(_fio);
//			_st->SetGroup(_gr);
//			_st->SetNum(5);
			_gr->AddStud(FindStud(_id));
		}
	}
	file.close();
}

void Dekanat::PrintGroupList(string File)
{
		ofstream file;
		file.open(File);
		if (file.is_open())
			 {
			for (int i = 0; i < GroupNum-1; i++)
				 {
				allGroups[i]->PrintGroup(file);
				}
			}
		file.close();
}




void Dekanat::PrintStudList(string File)
{
	ofstream file;
	file.open(File);
	int* arr = new int[10];
	if (file.is_open())
	{
		for (int i = 0; i < StudNum-1; i++)
		{
			file << allStudents[i]->GetID() <<" " << allStudents[i]->getFio() << " " << allStudents[i]->getGroup()->GetTitle() << endl;
			allStudents[i]->PrintMarks(file);
			file << endl;
		}
		file.close();
	}
}


void Dekanat:: getStats()
{
	if(StudNum==0||GroupNum==0)
		cout<<"Error"<<endl;
	double arr[10];
	double av=0;
	int max = 0;
	double arr2[200];
	av=0; max=0;
	for(int i=0; i<GroupNum;i++)
	{
		arr[i]=allGroups[i]->getAvMark();
		av+=arr[i];
		if(arr[max]<arr[i])
			max=i;
	}
	cout<<"The highest average group grade is "<<arr[max]<<endl;
	cout<<"The most sucsessful group is "<<allGroups[max]->GetTitle()<<endl;
	cout<<"The average grade among the groups is "<<av/(double)GroupNum<<endl;
	for(int i=0; i<StudNum;i++)
	{
		arr2[i]=allStudents[i]->getAvMark();
		av+=arr2[i];	
		if(arr2[max]<arr2[i])
			max=i;
	}
	cout<<"The highest average student grade is "<<arr2[max]<<endl;
	cout<<"The most sucsessful student is "<<allStudents[max]->getFio()<<endl;
	cout<<"The average grade among the groups is "<<av/(double)StudNum<<endl;

}
void Dekanat:: MoveStud(int _id, Group* _group)
{
	Student* st = FindStud(_id);
	Group* gr = st->getGroup();
	gr->RemoveStud(_id);
	st->SetGroup(_group);
}
void Dekanat:: RemoveStud(int _id)
{
	Student* st = FindStud(_id);
	Group* gr = st->getGroup();
	gr->RemoveStud(_id);
	StudNum--;
}
void Dekanat::ChooseHead(int _num, int _id)
{
	if (FindStud(_id)->getGroup() == allGroups[_num])
		allGroups[_num]->SetHead(FindStud(_id));
	else
		cout << "Error: wrong student";
}
void Dekanat::PrintAll(void)
{
	for (int i = 0; i < GroupNum; i++)
		allGroups[i]->PrintGroup(cout);
}
void Dekanat::PrintAllStudents()
{
	for (int i = 0; i < StudNum; i++)
		allStudents[i]->PrintStudent();
}

void Dekanat::RemoveBadStudents(int _mark = 2.5)
{
	for (int i = 0; i < StudNum; i++)
	{
		if (allStudents[i]->getAvMark() <= _mark)
			RemoveStud(allStudents[i]->GetID());
	}
}

void Dekanat::ChooseHeadbyMarks(int _num)
{
	double max = 0;
	int number = -1;
	if (allGroups[_num]->GetNum() == 0)
		cout << "Error: empty group" << endl;
	else
	{
		for (int i=0; i<allGroups[_num]->GetNum(); i++)
			if (allGroups[_num]->getOnesAvMark(i) > max)
			{
				max = allGroups[_num]->getOnesAvMark(i);
				number = i;
			}
		ChooseHead(_num, allGroups[_num]->GetIdbyNum(number));
	}
}